variable "aws_region" {
  default = "us-east-1"
}

variable "ami-tag" {
  description = "AMI tag"
  type        = string
  default     = "latest"
}

variable "vpc_name" {
  description = "Le nom du VPC où déployer les ressources."
  type        = string
  default     = "application-build-VPC"
}

variable "public_subnet_a" {
  description = "Le nom du premier subnet public pour le Load Balancer."
  type        = string
  default     = "application-build-PublicSubnet-1"
}

variable "public_subnet_b" {
  description = "Le nom du deuxième subnet public pour le Load Balancer."
  type        = string
  default     = "application-build-PublicSubnet-2"
}

variable "private_subnet_a" {
  description = "Le nom du premier subnet privé pour les instances dans le groupe d'Auto Scaling."
  type        = string
  default     = "application-build-PrivateSubnet-1"
}

variable "private_subnet_b" {
  description = "Le nom du deuxième subnet privé pour les instances dans le groupe d'Auto Scaling."
  type        = string
  default     = "application-build-PrivateSubnet-2"
}

variable "security_group_name" {
  description = "Le nom du groupe de sécurité à attacher à l'ALB et aux instances EC2."
  type        = string
  default     = "application-build-SecurityGroup"
}

variable "instance_type" {
  description = "Le type d'instance EC2 à utiliser pour le groupe d'Auto Scaling."
  type        = string
  default     = "t2.micro"
}

variable "environment" {
  description = "L'environnement de déploiement (ex: dev, prod)."
  type        = string
  default     = "dev"
}

variable "listener_port" {
  description = "Port number for the load balancer listener"
  type        = number
  default     = 8080
}

resource "aws_sns_topic" "autoscaling_notifications" {
  name = "autoscaling-notifications"
}



