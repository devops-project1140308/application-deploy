provider "aws" {
  region = var.aws_region
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0.0"
    }
  }

  backend "http" {
    address          = "https://gitlab.com/api/v4/projects/55503741/terraform/state/tfstate"
    lock_address     = "https://gitlab.com/api/v4/projects/55503741/terraform/state/tfstate/lock"
    unlock_address   = "https://gitlab.com/api/v4/projects/55503741/terraform/state/tfstate/lock"
    username         = "Alenas-c"
    password         = "glpat-vzGNJXBdWm49_7Us6wk-"
    lock_method      = "POST"
    unlock_method    = "DELETE"
    retry_wait_min   = 5
  }
}


# Recherche du groupe de sécurité par son nom
data "aws_security_group" "selected" {
  filter {
    name   = "tag:Name"
    values = [var.security_group_name]
  }
}

# Recherche de l'AMI par nom
data "aws_ami" "selected" {
  most_recent = true
  filter {
    name   = "tag:Name"
    values = [var.ami-tag]
  }
  owners = ["self"]
}

# Recherche des subnets
data "aws_subnet" "public_a" {
  filter {
    name   = "tag:Name"
    values = [var.public_subnet_a]
  }
}

data "aws_subnet" "public_b" {
  filter {
    name   = "tag:Name"
    values = [var.public_subnet_b]
  }
}

data "aws_subnet" "private_a" {
  filter {
    name   = "tag:Name"
    values = [var.private_subnet_a]
  }
}

data "aws_subnet" "private_b" {
  filter {
    name   = "tag:Name"
    values = [var.private_subnet_b]
  }
}

# Configuration du Load Balancer
resource "aws_lb" "app_lb" {
  name               = "${var.ami-tag}-${var.environment}-app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [data.aws_security_group.selected.id]
  subnets            = [data.aws_subnet.public_a.id, data.aws_subnet.public_b.id]
  enable_deletion_protection = false
}

resource "aws_lb_target_group" "tg" {
  name     = "${var.ami-tag}-${var.environment}-tg"
  port     = var.listener_port
  protocol = "HTTP"
  vpc_id   = data.aws_security_group.selected.vpc_id

  health_check {
    enabled             = true
    path                = "/"
    protocol            = "HTTP"
    healthy_threshold   = 3
    unhealthy_threshold = 3
    timeout             = 5
    interval            = 30
    matcher             = "200"
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.app_lb.arn
  port              = var.listener_port
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

# Modèle de lancement pour le groupe d'Auto Scaling
resource "aws_launch_template" "app_lt" {
  name_prefix   = "${var.ami-tag}-${var.environment}-lt-"
  image_id      = data.aws_ami.selected.id
  instance_type = var.instance_type
  vpc_security_group_ids = [data.aws_security_group.selected.id]

  user_data = base64encode(<<-EOF
                      #!/bin/bash
                      echo "Hello, World!"
                    EOF
  )

  lifecycle {
    create_before_destroy = true
  }
}

# Groupe d'Auto Scaling avec instance refresh configuré
resource "aws_autoscaling_group" "app_asg" {
  launch_template {
    id      = aws_launch_template.app_lt.id
    version = "$Latest"
  }

  min_size             = 2
  max_size             = 5
  desired_capacity     = 2
  vpc_zone_identifier  = [data.aws_subnet.private_a.id, data.aws_subnet.private_b.id]
  target_group_arns    = [aws_lb_target_group.tg.arn]

  tag {
    key                 = "Name"
    value               = "${var.ami-tag}-${var.environment}-instance"
    propagate_at_launch = true
  }

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
      instance_warmup        = 300
    }
  }
}
